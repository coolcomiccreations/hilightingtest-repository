﻿using UnityEngine;
using System.Collections;

public class materialPickUp : MonoBehaviour {
	public string matName;
	public int amount;

	GameObject flyTarget;
	materialsDatabase targetBase;

	bool flying = false;
	float destroyDistance = .8f;
	float timeSinceFly = 0;

	float maxFlyTime = 1f;


	public void flyToTarget(GameObject t)
	{
		if (!flying) {
			Collider coll = GetComponentInParent<Collider>();
			if (coll !=null)
				coll.isTrigger = true;
			flyTarget = t;
			targetBase = flyTarget.GetComponentInChildren<materialsDatabase> ();
			flying = true;
			timeSinceFly = 0;
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if(flying)
		{
			timeSinceFly+= Time.fixedDeltaTime;
			transform.position = Vector3.Lerp(transform.position,flyTarget.transform.position,timeSinceFly/maxFlyTime);
			if (Vector3.Distance(transform.position, flyTarget.transform.position) <= destroyDistance)
			{
				targetBase.addMaterial(matName,amount);
				Debug.Log("Mat Added: " + matName+" "+ targetBase.checkAmount(matName));
				Destroy(gameObject);
			}
		}
	}
}

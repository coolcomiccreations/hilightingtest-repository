﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class highlighter : MonoBehaviour {

    public enum highlightModes {mouse, distance, camera}

    bool pickingUp = false;

    public highlightModes HighlighterMode;
    public GameObject materialsDB;

    GameObject testobj;
    GameObject cylinder;
    

    bool highlighting = true;
    public GameObject oldTarget;
    int hitmask;

    public void pickUp()
    {
        if (oldTarget)
        {
            materialPickUp mp = oldTarget.GetComponent<materialPickUp>();
            if (mp)
            {
                mp.flyToTarget(materialsDB);
            }
        }
    }

	// Use this for initialization
	void Start () {
        cylinder = (GameObject)Instantiate(Resources.Load("highlighter"));
        testobj = gameObject;
        cylinder.SetActive(true);
        hitmask = 1 << 8 + 1<<7 + 1<<6 + 1<<5 + 1<<9 + 1; //8 is interactable
    }

    Renderer getLargestRend(GameObject target)
    {
        Renderer[] rends = target.GetComponentsInChildren<Renderer>();
        float maxsize = 0;
        Renderer largest = null;
        foreach (Renderer r in rends)
        {
            Vector3 size = r.bounds.size;
            if(size.magnitude> maxsize)
            {
                largest = r;
                maxsize = size.magnitude;
            }
        }
        return largest;
    }

    void highlight(GameObject target)
    {
        if(target == null)
        {
            cylinder.SetActive(false);
            return;
        }
        cylinder.SetActive(true);
        if (target == oldTarget)
        {
            Debug.Log("ret same");
            cylinder.transform.position = target.transform.position;
            return;
        }
        Renderer rend = getLargestRend(target);
        if (rend == null)
        {
            Debug.Log("ret null");
            return;
        }


        Vector3 size = rend.bounds.size;
        cylinder.transform.localScale = size;
        cylinder.transform.position = target.transform.position;
        oldTarget = target;
        Debug.Log("changed highlight");

    }

    List<GameObject> objs = new List<GameObject>();
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 8)
        {
            objs.Add(other.gameObject);
        }
        /*
        materialPickUp obj = other.gameObject.GetComponentInChildren<materialPickUp>();
        if (obj != null)
        {

            obj.flyToTarget(gameObject);

        }
        */
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            objs.Remove(other.gameObject);
        }
    }

    void closestObj()
    {
        if(objs.Count > 0)
        {
            float mindist = 10000;
            GameObject closest = null;
            foreach(GameObject gobj in objs)
            {
                if(gobj == null)
                {
                    continue;
                }
                float mag = (gobj.transform.position - transform.position).magnitude;
                if ( mag< mindist)
                {
                    mindist = mag;
                    closest = gobj;
                }
            }
            
            highlight(closest);
        }
        else
        {
            cylinder.SetActive(false);
        }
    }

    void findTarget()
    {
        Camera cam = Camera.allCameras[0];
        Ray r = cam.ScreenPointToRay(new Vector3(cam.pixelWidth/2f, cam.pixelHeight / 2f, 0));
        Debug.DrawLine(r.origin, r.origin + (r.direction * 100));
        //Debug.Log("r "+r.direction.x + " " + r.direction.y + " " + r.direction.z + " ");
        RaycastHit rh;
        if (Physics.Raycast(r, out rh, 10000,(1<<8)))
        {
            Debug.Log("Hit");
            highlight(rh.collider.gameObject);
        }
        else
        {
            cylinder.SetActive(false);
        }
    }

    void findTargetMouse()
    {
        Camera cam = Camera.allCameras[0];
        Ray r = cam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawLine(r.origin, r.origin + (r.direction * 100));
        //Debug.Log("r "+r.direction.x + " " + r.direction.y + " " + r.direction.z + " ");
        RaycastHit rh;
        if (Physics.Raycast(r, out rh, 10000, (1 << 8)))
        {
            Debug.Log("Hit");
            highlight(rh.collider.gameObject);
        }
        else
        {
            cylinder.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update () {
        if(HighlighterMode == highlightModes.camera)
        {
            findTarget();
        }
        if(HighlighterMode == highlightModes.distance)
        {
            closestObj();
        }
        if(HighlighterMode == highlightModes.mouse)
        {
            findTargetMouse();
        }
        
        
    }
}

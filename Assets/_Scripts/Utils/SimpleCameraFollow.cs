﻿//@udoddvadym

using UnityEngine;

public class SimpleCameraFollow : MonoBehaviour
{
    public Transform target;

    void FixedUpdate()
    {
        transform.LookAt(target);
    }
}

﻿//@vadym udod

using UnityEngine;

public class SimpleFollowDelayed : MonoBehaviour
{
    public Transform Target;
    public float FollowSpeed = .1f;
    public float RotationSpeed = .1f;
    public float RotateThresshold = .75f;

    void Start()
    {
        if (!Target)
        {
            Target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }
    }

    private float hdir = 0;
    private float vdir = 0;
    void Update()
    {
        hdir = Input.GetAxisRaw("Horizontal");
        vdir = Input.GetAxisRaw("Vertical");
    }

    void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, Target.position, Time.fixedDeltaTime * FollowSpeed);
        //transform.position = Target.position;
        if (Mathf.Abs(U.JoystickData.x) > RotateThresshold || Mathf.Abs(hdir) > RotateThresshold)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Target.forward, Vector3.up), Time.fixedDeltaTime * RotationSpeed);
            //transform.rotation = Quaternion.LookRotation(Target.forward, Vector3.up);
            /*
            if ((transform.rotation.ToEulerAngles() - Quaternion.LookRotation(Target.forward, Vector3.up).ToEulerAngles()).magnitude < 1)
            {

            }
            */
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class materialsDatabase : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
        if (other)
        {
            materialPickUp obj = other.gameObject.GetComponentInChildren<materialPickUp>();
            if (obj != null)
            {

                //obj.flyToTarget(gameObject);

            }
        }
	}

	Dictionary<string,int> materialCollection = new Dictionary<string, int>();

	public void addMaterial(string name, int amount)
	{
		if (materialCollection.ContainsKey (name)) {
			materialCollection[name] += amount;

		} else {
			materialCollection.Add(name,amount);
		}
	}

	/*
	 * Returns true if you have enough of the material. 
	 * param: removeOnlyIfEnough effects if it removes the amount specified if there isn't enough
	 * if you want to remove 50, to pay for construction, then you would set it to true (the default) and
	 * it will only remove 50 if you have enough otherwise it will return false and do nothing.
	 * If removeOnlyIfEnough is false when you call it, and there isn't enough, it will remove until its 0. 
	 * this could be used if you want enemy attacks to remove resources for example.
	 * */
	public bool removeMaterial(string name, int amount, bool removeOnlyIfEnough = true)
	{
		bool enough = false;
		if (materialCollection.ContainsKey (name)) {
			if (materialCollection[name] >= amount)
			{
				enough = true;
				materialCollection[name] -= amount;
			}
			else if(!removeOnlyIfEnough)
			{
				materialCollection[name] = 0;
			}

		} else {
			if(!removeOnlyIfEnough)
				materialCollection.Add(name, 0);
		}

		return enough;
	}

	public int checkAmount(string name)
	{
		if (materialCollection.ContainsKey (name)) {
			return materialCollection [name];
		} else {
			materialCollection.Add(name, 0);
			return 0;
		}

	}

    /*
	// Use this for initialization
	void Start () 
	{
		
	}
    */
}

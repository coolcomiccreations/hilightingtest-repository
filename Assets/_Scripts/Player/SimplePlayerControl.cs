﻿//@vadym udod

using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class SimplePlayerControl : MonoBehaviour
{
    #region Variables
    public float JumpStrength = 10f;    //jump strength
    //public float DistanceToGround = .1f;//used to check if player is grounded. Bigger the value is, further player needs to be to be able to jump
    public float MoveSpeed = .1f;       //player move speed value
    public float TurnSpeed = 10f;       //player turn speed
    public float JumpCheckTime = .3f;   //after a jump, wait for .3 seconds, and start jump checks
    public float InAirSpeed = .1f;      //movement and rotation speed are multiplied by this value when in air 
    public Animator anim;
    public GameObject camera;
    public List<Component> componentsToDisable;

    private bool acting = false;
    private Rigidbody rBody;            //player's rigidbody
    private Vector3 lookAtTarget;       //player lookAt target, that will be rotated when turning the player
    private bool canJump;               //can player jump?
    private bool canCheckJump;          //can script do jump check?
    private bool lastJumpCheck;
    private float playerHeight;         //player's collider height
    private float playerWidth;
    private Vector3 inAirDirection;     //player's direction before jump
    private float hdir = 0;
    private float vdir = 0;
    Vector3 movement;
    Rigidbody playerRigidbody;
    public bool grounded;

    private Transform cameraTransform;
    //private PhotonView view;
    #endregion

    #region Methods
    void Start()
    {
        //view = GetComponent<PhotonView>();
        //instantiate only if owner
        //if (view.isMine)
        //{
            //create camera for itsel
            Instantiate(camera);
            
            rBody = GetComponent<Rigidbody>();
            playerHeight = GetComponent<Collider>().bounds.extents.y;
            playerWidth = GetComponent<Collider>().bounds.extents.x;
            cameraTransform = Camera.main.transform;
        //}
        /*
        else
        {
            foreach(Component component in componentsToDisable)
            {
                Destroy(component);
            }
        }
        */
        canCheckJump = true;
        lastJumpCheck = true;
        playerRigidbody = GetComponent<Rigidbody>();
    }

    //Simpler move, but don't need it
    void Move(float h, float v, float speed)
    {
        bool grounded = isGrounded();

        animControl();

        movement.Set(h, 0, v);
        movement = movement.normalized * speed * Time.fixedDeltaTime; //wait shouldn't it be fixed time?
        playerRigidbody.MovePosition(transform.position + movement);

        if (canCheckJump && grounded)//restore jump every time player can jump
        {
            canJump = true;
        }

        lastJumpCheck = grounded;
    }

    void FixedUpdate()
    {

        


        if (true)//(view.isMine)
        {
            grounded = isGrounded();
            Vector3 resultMove = transform.position;
            Quaternion resultRotation = transform.rotation;

            //animControl();

            if (!grounded && lastJumpCheck)
            {
                inAirDirection = transform.forward * U.JoystickData.magnitude;
                canJump = false;
            }
            else if (grounded && !lastJumpCheck)
            {
                inAirDirection = Vector3.zero;
            }

            if (!grounded)
            {
                resultMove += inAirDirection * MoveSpeed;
            }

    //#if UNITY_EDITOR
            if (hdir != 0f || vdir != 0f)
            {
                //use axis for tests only
                lookAtTarget = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0) * new Vector3(hdir, 0f, vdir);

                if (grounded)
                {
                    //try with addforce later
                    resultMove += transform.forward * MoveSpeed;
                    resultRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookAtTarget, Vector3.up), TurnSpeed);
                }
                else
                {
                    resultMove += transform.forward * MoveSpeed * InAirSpeed;
                    resultRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookAtTarget, Vector3.up), TurnSpeed * InAirSpeed);
                }
            }
//#else
/*
            if (U.JoystickData.magnitude > 0f)//if user iteract with joystick
            {
                //make movement direction relative to camera's view
                lookAtTarget = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0) * new Vector3(U.JoystickData.x, 0f, U.JoystickData.y);//get a vector3 that points joystick direction

                if (grounded)
                {
                    //try with addforce later
                    resultMove += transform.forward * MoveSpeed * U.JoystickData.magnitude;
                    resultRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookAtTarget, Vector3.up), TurnSpeed);
                }
                else
                {
                    resultMove += transform.forward * MoveSpeed * InAirSpeed;
                    resultRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lookAtTarget, Vector3.up), TurnSpeed * InAirSpeed);
                }
            }
            */
//#endif
           
            transform.position = resultMove;
            transform.rotation = resultRotation;

            if (canCheckJump && grounded)//restore jump every time player can jump
            {
                canJump = true;
            }

            lastJumpCheck = grounded;
        }
        
    }

    public highlighter HL;
    public void pickUpButton()
    {
        if(!acting && HL.oldTarget)
        {
            Invoke("pickUpAfter", 1.6f);
            acting = true;
        }
    }
    void pickUpAfter()
    {
        acting = false;
        HL.pickUp();
    }


    void Update()
    {
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

        hdir = Input.GetAxisRaw("Horizontal");
        vdir = Input.GetAxisRaw("Vertical");
        if(acting)
        {
            hdir = 0;
            vdir = 0;
        }
        

        if (stateInfo.IsName("Base Layer.Jump"))
        {
            anim.SetBool("jump", false);
        }

        if (Input.GetKeyDown(KeyCode.Space) )// && view.isMine)
        {
            tryJump();
        }
    }

    private void animControl()
    {
        anim.SetFloat("speed", U.JoystickData.magnitude);
        if (U.JoystickData.magnitude == 0f)
        {
            anim.speed = 1f;
        }
        else
        {
            //i know raw numbers arent suppose to be in this part of code, but its just for animation test
            if (U.JoystickData.magnitude > 0f && U.JoystickData.magnitude < 0.4f)
            {
                anim.speed = U.JoystickData.magnitude / 0.4f;
            }
            else if (U.JoystickData.magnitude > 0.4f)
            {
                anim.speed = U.JoystickData.magnitude;
            }

        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (!isGrounded() || !canJump)
        {
            inAirDirection = Vector3.zero;
        }
    }

    public void tryJump()
    {
        if (canJump)
        {
            //do jump
            anim.SetBool("jump", true);
            
            rBody.AddForce(Vector3.up * JumpStrength);
            canJump = false;
            canCheckJump = false;
            StartCoroutine(jumpCheckDelay(JumpCheckTime));//start timer for jump checks
        }
    }

    public bool isGrounded()
    {
        return Physics.Raycast(transform.position + new Vector3(0,1.1f,0), Vector3.down, 1.3f);//cast ray beneth the player, to see is there something under
    }

    public bool isRunningIntoWall()
    {
        return Physics.Raycast(transform.position, transform.forward, playerWidth/* + DistanceToWall*/);
    }

    //timers
    private IEnumerator jumpCheckDelay(float time)
    {
        yield return new WaitForSeconds(time);//wait for #time
        canCheckJump = true;//and restore jump checks
    }
#endregion
}
